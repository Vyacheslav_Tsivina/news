package com.epam.news.service;

import com.epam.news.entity.NewsVO;
import com.epam.news.exception.ServiceException;
/**
 * Interface that provides complex actions with news
 *
 */
public interface INewsManageService {
	/**
	 * Add news with news info, author info, tags and comments
	 * @param newsVO to add
	 * @throws ServiceException
	 */
	void add(NewsVO newsVO) throws ServiceException;

	/**
	 * Add news tags. Doesn't add tag if it already exist
	 * @param newsVO to be added
	 * @throws ServiceException
	 */
	void addTags(NewsVO newsVO) throws ServiceException;

	/**
	 * Delete news according news id and all information about it(tags,comments)
	 * @param newsVO to be deleted
	 * @throws ServiceException
	 */
	void delete(NewsVO newsVO) throws ServiceException;
	/**
	 * 
	 * @param id of news
	 * @return NewsVO(news,author,tags,comments) object with given id
	 * @throws ServiceException
	 */
	NewsVO findNewsVO(Long id) throws ServiceException;
}
