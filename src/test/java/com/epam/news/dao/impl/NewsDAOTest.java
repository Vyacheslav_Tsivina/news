package com.epam.news.dao.impl;

import java.io.FileInputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.impl.NewsDAOImpl;
import com.epam.news.entity.Author;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
/**
 * Test C.R.U.D and other operations from NewsDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-configuration-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
	TransactionDbUnitTestExecutionListener.class,
	DirtiesContextTestExecutionListener.class,
	DbUnitTestExecutionListener.class })
@Transactional
public class NewsDAOTest extends DBTestCase
{
	private final static String FULL_DB_PATH="src/test/resources/com/epam/news/dao/fullDB.xml";
	private static final String NEWS_DAO = "newsDAO";

	private static Logger logger= Logger.getLogger(NewsDAOTest.class);
	private static BeanFactory factory = new ClassPathXmlApplicationContext("classpath:spring-configuration-test.xml");

	/**
	 * Configuration of log4j 
	 */
	static
	{
		ResourceBundle bundle = ResourceBundle.getBundle("resources.path",new Locale("en","EN"));
		PropertyConfigurator.configure(bundle.getString("log4j.properties"));
	}
	/**
	 * Set properties for DBUnit
	 */
	public NewsDAOTest(){
		super( );
		ResourceBundle bundle = ResourceBundle.getBundle("resources.database",new Locale("en","EN"));

		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, bundle.getString("db.driver"));
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, "jdbc:oracle:thin:@localhost");
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, bundle.getString("db.test.login"));
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, bundle.getString("db.test.password"));
		System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, bundle.getString("db.test.schema") );
	}
	/**
	 * Set DB state
	 */
	@Before
	public void setDB()
	{
		try {
			setUp();
		} catch (Exception e) {
			fail();
			logger.error(e);
		}
	}
	protected IDataSet getDataSet() throws Exception
	{
		return new FlatXmlDataSetBuilder().build(new FileInputStream(FULL_DB_PATH));
	}
	/**
	 * Test add method
	 */
	@Test
	public void testAdd() 
	{
		try {
			News news = new News(15,"q","q","q",new Timestamp(1427087631840L),new Timestamp(1427087631840L));

			NewsDAOImpl newsDAO = (NewsDAOImpl) factory.getBean(NEWS_DAO);
			long id = newsDAO.add(news);
			news.setId(id);

			News actualNews = newsDAO.findById(news.getId());

			Assert.assertEquals(news, actualNews);	
		}  catch (Exception e) {
			logger.equals(e);
			fail("Exception");
		}
	}
	/**
	 * Test findAll Method
	 */
	@Test
	public void testFindAll()
	{
		try{
			NewsDAOImpl newsDAO = (NewsDAOImpl) factory.getBean(NEWS_DAO);
			List<News> news = newsDAO.findAll();
			Assert.assertEquals(9, news.size());
		}catch (Exception e) {
			logger.equals(e);
			fail("Exception");
		}
	}
	/**
	 * Test findById method
	 */
	@Test
	public void testFindById()
	{
		try{
			NewsDAOImpl newsDAO = (NewsDAOImpl) factory.getBean(NEWS_DAO);
			News news = newsDAO.findById(1L);
			Assert.assertNotNull(news);
		}catch (Exception e) {
			logger.equals(e);
			fail("Exception");
		}
	}
	/**
	 * Test if delete successful
	 */
	@Test
	public void testDeleteNewsSuccess()
	{
		try{
			News news = new News(15,"q","q","q",new Timestamp(1427087631840L),new Timestamp(1427087631840L));

			NewsDAOImpl newsDAO = (NewsDAOImpl) factory.getBean(NEWS_DAO);
			long id = newsDAO.add(news);
			boolean flag = newsDAO.delete(id);
			Assert.assertTrue(flag);//if delete successful
		}catch (Exception e) {
			logger.equals(e);
			fail("Exception");
		}
	}
	/**
	 * Test update method
	 */
	@Test
	public void testUpdate()
	{
		try{
			News news = new News(1,"q","q","q",new Timestamp(1427087631840L),new Timestamp(1427087631840L));
			NewsDAOImpl newsDAO = (NewsDAOImpl) factory.getBean(NEWS_DAO);
			boolean flag = newsDAO.update(news);
			Assert.assertTrue(flag);//if update successful
		}catch (Exception e) {
			logger.equals(e);
			fail("Exception");
		}
	}
	/**
	 * Test find news by author
	 */
	@Test
	public void testFindByAuthor()
	{
		try{
			Author author = new Author(1L,"",null);
			NewsDAOImpl newsDAO = (NewsDAOImpl) factory.getBean(NEWS_DAO);

			List<News> news = newsDAO.findByAuthor(author);
			Assert.assertEquals(3, news.size());
		}catch (Exception e) {
			logger.equals(e);
			fail("Exception");
		}
	}
	/**
	 * Test find news by tags
	 */
	@Test
	public void testFindByTags()
	{
		try{
			List<Tag> tags = new ArrayList<>();
			tags.add(new Tag(22,""));
			tags.add(new Tag(21,""));
			NewsDAOImpl newsDAO = (NewsDAOImpl) factory.getBean(NEWS_DAO);
			
			List<News> news = newsDAO.findByTags(tags);
			Assert.assertEquals(2, news.size());
		}catch (Exception e) {
			logger.equals(e);
			fail("Exception");
		}
	}
	/**
	 * Test most commented news
	 */
	@Test
	public void testMostCommented()
	{
		try{

			NewsDAOImpl newsDAO = (NewsDAOImpl) factory.getBean(NEWS_DAO);
			List<News> news = newsDAO.mostCommentedNews();
			Assert.assertEquals(9, news.get(0).getId());//if news in right order(7 news have the biggest amount of comments in test DB)
			Assert.assertEquals(9, news.size());
		}catch (Exception e) {							
			logger.equals(e);
			fail("Exception");
		}
	}
	/**
	 * Test news by page
	 */
	@Test
	public void testNewsByPage()
	{
		try{

			NewsDAOImpl newsDAO = (NewsDAOImpl) factory.getBean(NEWS_DAO);
			List<News> news = newsDAO.findNewsByPage(5L, 1L);//find first 5 news
			Assert.assertEquals(5, news.size());

			news = newsDAO.findNewsByPage(5L, 100L);// news from 500 to 600
			Assert.assertEquals(0, news.size());
		}catch (Exception e) {
			logger.equals(e);
			fail("Exception");
		}
	}

}