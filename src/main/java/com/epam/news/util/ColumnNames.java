package com.epam.news.util;

public class ColumnNames {
	private ColumnNames(){};
	public static final String AUTHOR_ID="author_id";
	public static final String AUTHOR_NAME="name";
	public static final String AUTHOR_EXPIRED="expired";
	
	public static final String COMMENT_ID="comment_id";
	public static final String COMMENT_TEXT="comment_text";
	public static final String CREATION_DATE="creation_date";
	public static final String NEWS_ID="news_id";
	
	public static final String SHORT_TEXT="short_text";
	public static final String FULL_TEXT="full_text";
	public static final String TITLE="title";
	public static final String MODIFICATION_DATE="modification_date";
	
	public static final String TAG_ID="tag_id";
	public static final String TAG_NAME="tag_name";
}
