package com.epam.news.service.impl;

import org.springframework.transaction.annotation.Transactional;

import com.epam.news.entity.NewsVO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IAuthorService;
import com.epam.news.service.ICommentService;
import com.epam.news.service.INewsManageService;
import com.epam.news.service.INewsService;
import com.epam.news.service.ITagService;

/**
 * Class that provides complex actions with news
 *
 */
@Transactional
public class NewsManageServiceImpl implements INewsManageService{
		
	private INewsService newsService;
	private ITagService tagService;
	private ICommentService commentService;
	private IAuthorService authorService;
	/**
	 * Add news with news info, author info, tags and comments
	 * @param newsVO to add
	 * @throws Exception 
	 */
	@Override
	public void add(NewsVO newsVO) throws ServiceException {
			newsService.addNews(newsVO.getNews());//create news

			for (Tag tag:newsVO.getTags())//create tags and find id's
			{
				tagService.addTag(tag);
			}
			newsService.insertNewsAuthor(
					newsVO.getNews().getId(), 
					newsVO.getAuthor().getId());//insert news~author info
			newsService.insertNewsTags(
					newsVO.getNews().getId(), 
					newsVO.getTags());//insert news~tags info

	}

	/**
	 * Add news tags. Doesn't add tag if it already exist
	 * @param newsVO to be added
	 */
	@Override
	public void addTags(NewsVO newsVO) throws ServiceException {
			for (Tag tag:newsVO.getTags())//create tags and find id's
			{
					tagService.addTag(tag);
			}
			newsService.insertNewsTags(
					newsVO.getNews().getId(),
					newsVO.getTags());

	}

	/**
	 * Delete news according news id and all information about it(tags,comments)
	 * @param newsVO to be deleted
	 */
	@Override
	public void delete(NewsVO newsVO) throws ServiceException {
			newsService.deleteNewsAuthor(newsVO.getNews().getId()); // delete news author
			newsService.deleteNewsTags(newsVO.getNews().getId()); // delete news tags
			commentService.deleteCommentsForNews(newsVO.getNews().getId()); // delete comments for news
			newsService.deleteNews(newsVO.getNews());// delete news	
	}

	/**
	 * 
	 * @param id of news
	 * @return NewsVO(news,author,tags,comments) object with given id
	 * @throws ServiceException
	 */
	@Override
	public NewsVO findNewsVO(Long id) throws ServiceException {
		NewsVO result = new NewsVO();
		result.setNews(newsService.viewSingleNews(id));
		result.setAuthor(authorService.findAuthor(id));
		result.setComments(commentService.findCommentsForNews(id));
		result.setTags(tagService.findTagsForNews(id));
		return result;
	}
	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}

	public void setTagService(ITagService tagService) {
		this.tagService = tagService;
	}

	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}

	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}
	
}
