package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;

/**
 * Interface that provides C.R.U.D. operations with News and related information
 *
 */
public interface INewsDAO {
	/**
	 * Find list of all news
	 * @return list of news
	 * @throws DAOException 
	 */
	List<News> findAll() throws DAOException;
	/**
	 * Find news by id
	 * @param id
	 * @return
	 * @throws DAOException
	 */
	News findById(Long id) throws DAOException;
	/**
	 * 
	 * @param id of news
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException 
	 */
	boolean delete(Long id) throws DAOException;
	/**
	 * Create news with given info
	 * @param entity
	 * @return id of inserted comment
	 * @throws DAOException 
	 */
	Long add(News entity) throws DAOException;
	/**
	 * Update news info by id
	 * @param entity
	 * @return true if update is successful, returns false otherwise
	 * @throws DAOException 
	 */
	boolean update(News entity) throws DAOException;
	/**
	 * Insert info about news author
	 * @param newsId 
	 * @param authorId 
	 * @return true if insert is successful, returns false otherwise
	 * @throws DAOException 
	 */
	boolean insertNewsAuthor(Long newsId,Long authorId) throws DAOException;
	/**
	 * Check if there is already such tag(tags) for news and then insert it if it needed
	 * @param newsId news id
	 * @param tags ArrayList of tags to be added
	 * @throws DAOException 
	 */
	void insertNewsTags(Long newsId, List<Tag> tags) throws DAOException;
	/**
	 * Find all news from given author
	 * @param author
	 * @return List of all news by author
	 * @throws DAOException 
	 */
	List<News> findByAuthor(Author author) throws DAOException;
	/**
	 * Find news by given tag
	 * @param tag
	 * @return return List of all news by tag
	 * @throws DAOException 
	 */
	List<News> findByTags(List<Tag> tags) throws DAOException;
	/**
	 * Deletes all tags for news 
	 * @param id of news
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException 
	 */
	boolean deleteNewsTags(Long id) throws DAOException;
	/**
	 * Delete news author by news id
	 * @param id of news
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException 
	 */
	boolean deleteNewsAuthor(Long id) throws DAOException;
	/**
	 * Method make list of all news sorted by number of comments
	 * @return List of news
	 * @throws DAOException 
	 */
	List<News> mostCommentedNews() throws DAOException;
	/**
	 * Return list of news on given page number
	 * pages number begins with 1. News ordered by creation date;
	 * @param newsOnPage how many news will be on page
	 * @param pageNumber determine number of page which should be return
	 * @return list of news on given page
	 * @throws DAOException
	 */
	List<News> findNewsByPage(Long newsOnPage, Long pageNumber) throws DAOException;

}
