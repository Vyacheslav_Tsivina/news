package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
/**
 * Interface that provides C.R.U.D. operations with Comment 
 *
 */
public interface ICommentDAO {
	/**
	 * Find all comments 
	 * @return List of comments
	 * @throws DAOException
	 */
	List<Comment> findAll() throws DAOException;
	/**
	 * Find comment by id
	 * @param id
	 * @return comment if find is successful, returns null otherwise
	 * @throws DAOException
	 */
	Comment findById(Long id) throws DAOException;
	/**
	 * Delete comment by id
	 * @param id of comment
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException
	 */
	boolean delete(Long id) throws DAOException;
	/**
	 * Create comment with given info
	 * @param entity
	 * @return id of inserted comment
	 * @throws DAOException
	 */
	Long add(Comment entity) throws DAOException;
	/**
	 * Update comment info by id
	 * @param entity
	 * @return true if update is successful, returns false otherwise
	 * @throws DAOException
	 */
	boolean update(Comment entity) throws DAOException;
	/**
	 * Deletes all comments for news with given id
	 * @param id of news
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException 
	 */
	boolean deleteCommentsForNews(Long id) throws DAOException;
	/**
	 * Find all comments for news with given id
	 * @param id of news
	 * @return List of comments
	 * @throws DAOException
	 */
	List<Comment> findCommentsForNews(Long id) throws DAOException;
}
