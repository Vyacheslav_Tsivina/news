package com.epam.news.dao.impl;

import java.io.FileInputStream;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
/**
 * Test C.R.U.D operations from AuthorDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-configuration-test.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
	TransactionDbUnitTestExecutionListener.class,
	DirtiesContextTestExecutionListener.class,
	DbUnitTestExecutionListener.class })
@Transactional
public class AuthorDAOTest extends DBTestCase{
	private static final String AUTHOR_DAO = "authorDAO";
	private final static String FULL_DB_PATH="src/test/resources/com/epam/news/dao/fullDB.xml";
	
	static Logger logger= Logger.getLogger(AuthorDAOTest.class);
	private static BeanFactory factory = new ClassPathXmlApplicationContext("classpath:spring-configuration-test.xml");
	
	
	/**
	 * Configuration of log4j 
	 */
	static
	{
		ResourceBundle bundle = ResourceBundle.getBundle("resources.path",new Locale("en","EN"));
		PropertyConfigurator.configure(bundle.getString("log4j.properties"));
	}
	/**
	 * Set properties for DBUnit
	 */
	public AuthorDAOTest(){
		super();
		ResourceBundle bundle = ResourceBundle.getBundle("resources.database",new Locale("en","EN"));

		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, bundle.getString("db.driver"));
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, "jdbc:oracle:thin:@localhost");
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, bundle.getString("db.test.login"));
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, bundle.getString("db.test.password"));
		System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, bundle.getString("db.test.schema") );
	}
	/**
	 * Set DB state
	 */
	@Before
	public void setDB()
	{
		try {
			setUp();
		} catch (Exception e) {
			fail();
			logger.error(e);
		}
	}
	protected IDataSet getDataSet() throws Exception
	{
		return new FlatXmlDataSetBuilder().build(new FileInputStream(FULL_DB_PATH));
	}
	/**
	 * Test add method
	 * @throws DAOException 
	 */
	@Test
	public void testAdd()
	{
		try {
			Author author = new Author(1L,"qwerty",null);

			IAuthorDAO authorDAO = (AuthorDAOImpl) factory.getBean(AUTHOR_DAO);
			long id =authorDAO.add(author);
			Assert.assertNotNull(authorDAO.findById(id));
		}  catch (Exception e) {
			logger.equals(e);
			fail("Exception");
		}
	}
	/**
	 * Test successful and unsuccessful edit 
	 */
	@Test
	public void testEdit()
	{
		try {
			Author author = new Author(1L,"qwerty",null);

			IAuthorDAO authorDAO = (AuthorDAOImpl) factory.getBean(AUTHOR_DAO);
			boolean flag =authorDAO.update(author);
			Assert.assertTrue(flag);

			author = new Author(-1L,"qwerty",null);
			flag =authorDAO.update(author);
			Assert.assertFalse(flag);
		}  catch (Exception e) {
			logger.equals(e);
			fail("Exception");
		}
	}
	/**
	 * Test successful and unsuccessful delete 
	 */
	@Test
	public void testDelete()
	{
		try {
			Author author = new Author(1L,"qwerty",null);

			IAuthorDAO authorDAO = (AuthorDAOImpl) factory.getBean(AUTHOR_DAO);
			long id = authorDAO.add(author);
			boolean flag =authorDAO.delete(id);
			Assert.assertTrue(flag);

			author = new Author(-1L,"qwerty",null);
			flag =authorDAO.delete(author.getId());
			Assert.assertFalse(flag);
		}  catch (Exception e) {
			logger.equals(e);
			fail("Exception");
		}
	}
	/**
	 * Test find method
	 */
	@Test
	public void testFind()
	{
		try {
			IAuthorDAO authorDAO = (AuthorDAOImpl) factory.getBean(AUTHOR_DAO);
			Author author =authorDAO.findById(1L);
			Assert.assertNotNull(author);
		}  catch (Exception e) {
			logger.equals(e);
			fail("Exception");
		}
	}
}
