BEGIN
	BEGIN
	   EXECUTE IMMEDIATE 'DROP TABLE News_Author';
	EXCEPTION
	   WHEN OTHERS THEN
	      IF SQLCODE != -942 THEN
	         RAISE;
	      END IF;
	END;
	
	BEGIN
	   EXECUTE IMMEDIATE 'DROP TABLE News_Tag';
	EXCEPTION
	   WHEN OTHERS THEN
	      IF SQLCODE != -942 THEN
	         RAISE;
	      END IF;
	END;
	
	BEGIN
	   EXECUTE IMMEDIATE 'DROP TABLE Comments';
	EXCEPTION
	   WHEN OTHERS THEN
	      IF SQLCODE != -942 THEN
	         RAISE;
	      END IF;
	END;
	
	BEGIN
	   EXECUTE IMMEDIATE 'DROP TABLE News';
	EXCEPTION
	   WHEN OTHERS THEN
	      IF SQLCODE != -942 THEN
	         RAISE;
	      END IF;
	END;
	
	BEGIN
	   EXECUTE IMMEDIATE 'DROP TABLE Tag';
	EXCEPTION
	   WHEN OTHERS THEN
	      IF SQLCODE != -942 THEN
	         RAISE;
	      END IF;
	END;
	
	
	
	BEGIN
	   EXECUTE IMMEDIATE 'DROP TABLE Author';
	EXCEPTION
	   WHEN OTHERS THEN
	      IF SQLCODE != -942 THEN
	         RAISE;
	      END IF;
	END;
END;

CREATE TABLE News_Author (
  news_author_id NUMBER(20),
  news_id NUMBER(20),
  author_id NUMBER(20),
  expired TIMESTAMP,
  constraint News_Author_PK PRIMARY KEY (news_author_id)
);
CREATE sequence News_Author_SEQ START WITH 40;

CREATE TABLE News (
  news_id NUMBER(20),
  short_text VARCHAR2(200),
  full_text VARCHAR2(2000),
  title VARCHAR2(70),
  creation_date TIMESTAMP,
  modification_date TIMESTAMP,
  constraint News_PK PRIMARY KEY (news_id)
);
CREATE sequence News_SEQ START WITH 40;

CREATE TABLE News_Tag (
  news_tag_id NUMBER(20),
  news_id NUMBER(20),
  tag_id NUMBER(20),
  constraint News_Tag_PK PRIMARY KEY (news_tag_id)
);
CREATE sequence News_Tag_SEQ START WITH 40;

CREATE TABLE Tag (
  tag_id NUMBER(20),
  tag_name VARCHAR2(30),
  constraint Tag_PK PRIMARY KEY (tag_id)
);
CREATE sequence Tag_SEQ START WITH 40;

CREATE TABLE Author (
  author_id NUMBER(20),
  name VARCHAR2(30),
  expired TIMESTAMP,
  constraint Author_PK PRIMARY KEY (author_id)
);
CREATE sequence Author_SEQ START WITH 40;

CREATE TABLE Comments (
  comment_id NUMBER(20),
  comment_text VARCHAR2(100),
  creation_date TIMESTAMP,
  news_id NUMBER(20),
  constraint Comments_PK PRIMARY KEY (comment_id)
);
CREATE sequence Comments_SEQ START WITH 40;

ALTER TABLE News_Author ADD CONSTRAINT News_Author_fk1 FOREIGN KEY (news_id) REFERENCES News(news_id);

ALTER TABLE News_Author ADD CONSTRAINT News_Author_fk2 FOREIGN KEY (author_id) REFERENCES Author(author_id);

ALTER TABLE News_Tag ADD CONSTRAINT News_Tag_fk1 FOREIGN KEY (news_id) REFERENCES News(news_id);

ALTER TABLE News_Tag ADD CONSTRAINT News_Tag_fk2 FOREIGN KEY (tag_id) REFERENCES Tag(tag_id);

ALTER TABLE Comments ADD CONSTRAINT Comments_fk1 FOREIGN KEY (news_id) REFERENCES News(news_id);