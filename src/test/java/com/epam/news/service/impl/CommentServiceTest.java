package com.epam.news.service.impl;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.news.dao.impl.CommentDAOImpl;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;
/**
 * test comment service methods
 */
public class CommentServiceTest {
	private static final String COMMENT_SERVICE = "commentService";
	private static Logger logger= Logger.getLogger(CommentServiceTest.class);
	private static BeanFactory factory = new ClassPathXmlApplicationContext("classpath:spring-configuration-test.xml");
	/**
	 * Configuration of log4j 
	 */
	static
	{
		ResourceBundle bundle = ResourceBundle.getBundle("resources.path",new Locale("en","EN"));
		PropertyConfigurator.configure(bundle.getString("log4j.properties"));
	}
	/**
	 * Test add comment
	 */
	@Test
	public void testAddComment()
	{
		CommentServiceImpl commentServise = (CommentServiceImpl) factory.getBean(COMMENT_SERVICE);
		CommentDAOImpl commentDAOImpl = mock(CommentDAOImpl.class);
		Comment comment = mock(Comment.class);
		try {
			when(commentDAOImpl.add(comment)).thenReturn(1L);

			commentServise.setCommentDAO(commentDAOImpl);

			commentServise.addComment(comment);

			verify(commentDAOImpl).add(comment);

		} catch (DAOException | ServiceException  e) {
			logger.error(e);
			fail("Exception");
		}
	}
	/**
	 * Test delete comment
	 */
	@Test
	public void testDeleteComment()
	{
		CommentServiceImpl commentServise = (CommentServiceImpl) factory.getBean(COMMENT_SERVICE);
		CommentDAOImpl commentDAOImpl = mock(CommentDAOImpl.class);
		Comment comment = mock(Comment.class);
		try {
			when(comment.getId()).thenReturn(1L);
			when(commentDAOImpl.delete(1L)).thenReturn(true);

			commentServise.setCommentDAO(commentDAOImpl);

			commentServise.deleteComment(comment);

			verify(commentDAOImpl).delete(1L);
		} catch (DAOException | ServiceException  e) {
			logger.error(e);
			fail("Exception");
		}
	}
}
