package com.epam.news.service.impl;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IAuthorService;
/**
 * Class that provides actions with authors
 *
 */
@Transactional
public class AuthorServiceImpl implements IAuthorService{
	
	private static Logger logger= Logger.getLogger(AuthorServiceImpl.class);
	private IAuthorDAO authorDAO ;

	/**
	 * Configuration of log4j 
	 */
	static
	{
		ResourceBundle bundle = ResourceBundle.getBundle("resources.path",new Locale("en","EN"));
		PropertyConfigurator.configure(bundle.getString("log4j.properties"));
	}

	/**
	 * Add new author and set inserted id into author
	 * @param author to be added
	 */
	@Override
	public void addAuthor(Author author) throws ServiceException {
		try{
			author.setId(authorDAO.add(author));
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}
	/**
	 * Delete given author by author id
	 * @param author to be deleted
	 */
	@Override
	public void deleteAuthor(Author author) throws ServiceException {
		try{
			authorDAO.delete(author.getId());
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	/**
	 * Update given author by author id
	 * @param author to be updated
	 * @throws ServiceException
	 */
	@Override
	public void updateAuthor(Author author) throws ServiceException {
		try{
			authorDAO.update(author);
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}
	/**
	 * Find author by id
	 * @param id
	 * @return author or null
	 * @throws ServiceException
	 */
	@Override
	public Author findAuthor(Long id) throws ServiceException {
		Author author = null;
		try{
			author = authorDAO.findById(id);
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return author;
	}
	public void setAuthorDAO(IAuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}

}
