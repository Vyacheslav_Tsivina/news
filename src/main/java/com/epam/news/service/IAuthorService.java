package com.epam.news.service;

import com.epam.news.entity.Author;
import com.epam.news.exception.ServiceException;

/**
 * Interface that provides actions with authors
 *
 */
public interface IAuthorService {
	/**
	 * Add new author and set inserted id into author
	 * @param author to be added
	 */
	void addAuthor(Author author) throws ServiceException;
	/**
	 * Delete given author by comment id
	 * @param author to be deleted
	 */
	void deleteAuthor(Author author) throws ServiceException;
	/**
	 * Update given author by comment id
	 * @param author to be updated
	 * @throws ServiceException
	 */
	void updateAuthor(Author author) throws ServiceException;
	/**
	 * Find author by id
	 * @param id
	 * @return author or null
	 * @throws ServiceException
	 */
	Author findAuthor(Long id) throws ServiceException;
}
