package com.epam.news.service.impl;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.INewsDAO;
import com.epam.news.entity.Author;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.INewsService;
/**
 * Class that provides actions with news
 *
 */
@Transactional
public class NewsServiceImpl implements INewsService{
	
	private static Logger logger= Logger.getLogger(NewsServiceImpl.class);
	private INewsDAO newsDAO;

	/**
	 * Configuration of log4j 
	 */
	static
	{
		ResourceBundle bundle = ResourceBundle.getBundle("resources.path",new Locale("en","EN"));
		PropertyConfigurator.configure(bundle.getString("log4j.properties"));
	}
	/**
	 * Add news and set inserted id into news
	 * @param news to be added
	 */
	@Override
	public void addNews(News news) throws ServiceException {
		try{
			news.setId(newsDAO.add(news));
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		
	}
	/**
	 * Delete given news by news id
	 * @param tag to be deleted
	 */
	@Override
	public void deleteNews(News news) throws ServiceException {
		try{
			newsDAO.delete(news.getId());
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		
	}
	/**
	 * Edit news information by news id
	 * @param news to be edited
	 */
	@Override
	public void editNews(News news) throws ServiceException {
		try{
			newsDAO.update(news);
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/**
	 * Find all news written by given author
	 * @param author by which the search is carried out
	 * @return list of news
	 */
	@Override
	public List<News> newsByAuthor(Author author) throws ServiceException {
		List<News> result = null;
		try{
			result = newsDAO.findByAuthor(author);
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

	/**
	 * Return list of news on given page number.
	 * Page numbers begin with 1. News ordered by creation date;
	 * @param newsOnPage how many news will be on page
	 * @param pageNumber determine number of page which should be return
	 * @return list of news on given page
	 */
	@Override
	public List<News> findNewsByPage(Long newsOnPage, Long pageNumber) throws ServiceException {
		List<News> result = null;
		try{
			result = newsDAO.findNewsByPage(newsOnPage, pageNumber);
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

	/**
	 * Find all news that contain given tag
	 * @param tag by which the search is carried out
	 * @return list of news
	 */
	@Override
	public List<News> newsByTags(List<Tag> tags) throws ServiceException {
		List<News> result = null;
		try{
			result = newsDAO.findByTags(tags);
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

	/**
	 * Find news by id
	 * @param id of news
	 * @return news by given id
	 */
	@Override
	public News viewSingleNews(Long id) throws ServiceException {
		News result = null;
		try{
			result = newsDAO.findById(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}

	/**
	 * Find all news and sort it by the number of comments
	 * @param author by which the search is carried out
	 * @return list of news
	 */
	@Override
	public List<News> mostCommented() throws ServiceException {
		List<News> result = null;
		try{
			result = newsDAO.mostCommentedNews();
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}
	
	/**
	 * Insert info about news author
	 * @param newsId 
	 * @param authorId 
	 * @return true if insert is successful, returns false otherwise
	 * @throws DAOException 
	 */
	@Override
	public boolean insertNewsAuthor(Long newsId, Long authorId)
			throws ServiceException {
		boolean insertFlag = false;
		try {
			insertFlag = newsDAO.insertNewsAuthor(newsId, authorId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return insertFlag;
	}
	/**
	 * Check if there is already such tag(tags) for news and then insert it if it needed
	 * @param newsId news id
	 * @param tags ArrayList of tags to be added
	 * @throws DAOException 
	 */
	@Override
	public void insertNewsTags(Long newsId, List<Tag> tags)
			throws ServiceException {
		try {
			newsDAO.insertNewsTags(newsId, tags);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		
	}
	public void setNewsDAO(INewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}
	/**
	 * 
	 * @param id of news
	 * @return true if delete is successful, returns false otherwise
	 * @throws ServiceException 
	 */
	@Override
	public boolean deleteNewsAuthor(Long id) throws ServiceException {
		
		boolean deleteFlag = false;
		try {
			deleteFlag = newsDAO.deleteNewsAuthor(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return deleteFlag;
	}
	
	/**
	 * Deletes all tags for news 
	 * @param id of news
	 * @return true if delete is successful, returns false otherwise
	 * @throws ServiceException 
	 */
	@Override
	public boolean deleteNewsTags(Long id) throws ServiceException {
		boolean deleteFlag = false;
		try {
			deleteFlag = newsDAO.deleteNewsAuthor(id);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return deleteFlag;
	}
	

	

}
