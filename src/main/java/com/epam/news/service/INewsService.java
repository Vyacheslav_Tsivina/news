package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
/**
 * Interface that provides actions with news
 *
 */
public interface INewsService {
	/**
	 * Add news and set inserted id into news
	 * @param news to be added
	 */
	void addNews(News news) throws ServiceException;
	/**
	 * Delete given news by news id
	 * @param tag to be deleted
	 */
	void deleteNews(News news) throws ServiceException;
	/**
	 * Edit news information by news id
	 * @param news to be edited
	 */
	void editNews(News news) throws ServiceException;

	/**
	 * Find all news written by given author
	 * @param author by which the search is carried out
	 * @return list of news
	 */
	List<News> newsByAuthor(Author author) throws ServiceException;

	/**
	 * Return list of news on given page number.
	 * Page numbers begin with 1. News ordered by creation date;
	 * @param newsOnPage how many news will be on page
	 * @param pageNumber determine number of page which should be return
	 * @return list of news on given page
	 */
	List<News> findNewsByPage(Long newsOnPage, Long pageNumber) throws ServiceException;

	/**
	 * Find all news that contain given tag
	 * @param tag by which the search is carried out
	 * @return list of news
	 */
	List<News> newsByTags(List<Tag> tags) throws ServiceException;

	/**
	 * Find news by id
	 * @param id of news
	 * @return news by given id
	 */
	News viewSingleNews(Long id) throws ServiceException;

	/**
	 * Find all news and sort it by the number of comments
	 * @param author by which the search is carried out
	 * @return list of news
	 * @throws ServiceException 
	 */
	List<News> mostCommented() throws ServiceException;
	
	/**
	 * Insert info about news author
	 * @param newsId 
	 * @param authorId 
	 * @return true if insert is successful, returns false otherwise
	 * @throws ServiceException 
	 */
	boolean insertNewsAuthor(Long newsId,Long authorId) throws ServiceException;
	
	/**
	 * Check if there is already such tag(tags) for news and then insert it if it needed
	 * @param newsId news id
	 * @param tags ArrayList of tags to be added
	 * @throws ServiceException 
	 */
	void insertNewsTags(Long newsId,List<Tag> tags) throws ServiceException;
	/**
	 * 
	 * @param id of news
	 * @return true if delete is successful, returns false otherwise
	 * @throws ServiceException 
	 */
	boolean deleteNewsAuthor(Long id) throws ServiceException;
	
	/**
	 * Deletes all tags for news 
	 * @param id of news
	 * @return true if delete is successful, returns false otherwise
	 * @throws ServiceException 
	 */
	boolean deleteNewsTags(Long id) throws ServiceException;
}
