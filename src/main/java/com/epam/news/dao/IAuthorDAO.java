package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
/**
 * Interface that realize C.R.U.D. operations with Author 
 *
 */
public interface IAuthorDAO {
	/**
	 * Find all authors 
	 * @return List of authors
	 * @throws DAOException
	 */
	List<Author> findAll() throws DAOException;
	/**
	 * Find author by id
	 * @param id
	 * @return author if find is successful, returns null otherwise
	 * @throws DAOException
	 */
	Author findById(Long id) throws DAOException;
	/**
	 * Delete author by id
	 * @param id
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException
	 */
	boolean delete(Long id) throws DAOException;
	/**
	 * Create author with given info
	 * @param entity
	 * @return id of inserted author
	 * @throws DAOException
	 */
	Long add(Author entity) throws DAOException;
	/**
	 * Update author info by id
	 * @param entity
	 * @return true if update is successful, returns false otherwise
	 * @throws DAOException
	 */
	boolean update(Author entity) throws DAOException;
}
