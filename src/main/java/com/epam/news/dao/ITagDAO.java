package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;

/**
 * Interface that provides C.R.U.D. operations with Tag
 *
 */
public interface ITagDAO {
	/**
	 * Find all tags 
	 * @return List of comments
	 * @throws DAOException
	 */
	List<Tag> findAll() throws DAOException;
	/**
	 * Find tag by id
	 * @param id
	 * @return tag if find is successful, returns null otherwise
	 * @throws DAOException
	 */
	Tag findById(Long id) throws DAOException;
	/**
	 * Delete tag by id
	 * @param id of comment
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException
	 */
	boolean delete(Long id) throws DAOException;
	/**
	 * Create tag with given info
	 * @param entity
	 * @return id of inserted tag
	 * @throws DAOException
	 */
	Long add(Tag entity) throws DAOException;
	/**
	 * Update tag info by id
	 * @param entity
	 * @return true if update is successful, returns false otherwise
	 * @throws DAOException
	 */
	boolean update(Tag entity) throws DAOException;
	/**
	 * Find all tags for news with given id
	 * @param id of news
	 * @return list of tags
	 * @throws DAOException
	 */
	List<Tag> findTagsForNews(Long id) throws DAOException;
}
