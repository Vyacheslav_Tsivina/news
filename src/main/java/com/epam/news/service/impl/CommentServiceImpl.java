package com.epam.news.service.impl;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.ICommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.ICommentService;
/**
 * Class that provides actions with comments
 *
 */
@Transactional
public class CommentServiceImpl implements ICommentService{
	
	private static Logger logger= Logger.getLogger(CommentServiceImpl.class);
	private ICommentDAO commentDAO;

	/**
	 * Configuration of log4j 
	 */
	static
	{
		ResourceBundle bundle = ResourceBundle.getBundle("resources.path",new Locale("en","EN"));
		PropertyConfigurator.configure(bundle.getString("log4j.properties"));
	}
	/**
	 * Add new comment and set inserted id into comment
	 * @param comment to be added
	 */
	@Override
	public void addComment(Comment comment) throws ServiceException {
		try{
			comment.setId(commentDAO.add(comment));
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}
	/**
	 * Delete given comment by comment id
	 * @param comment to be deleted
	 */
	@Override
	public void deleteComment(Comment comment) throws ServiceException {
		try{
			commentDAO.delete(comment.getId());
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}
	/**
	 * Update given comment by comment id
	 * @param comment to be updated
	 * @throws ServiceException
	 */
	@Override
	public void updateComment(Comment comment) throws ServiceException {
		try{
			commentDAO.update(comment);
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		
	}
	/**
	 * Find comment by id
	 * @param id
	 * @return Comment or null
	 * @throws ServiceException
	 */
	@Override
	public Comment findComment(Long id) throws ServiceException {
		Comment comment = null;
		try{
			comment = commentDAO.findById(id);
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return comment;
	}
	
	/**
	 * Deletes all comments for news with given id
	 * @param id of news
	 * @return true if delete is successful, returns false otherwise
	 * @throws ServiceException 
	 */
	@Override
	public void deleteCommentsForNews(Long id) throws ServiceException {
		try{
			commentDAO.deleteCommentsForNews(id);
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	
	@Override
	public List<Comment> findCommentsForNews(Long id) throws ServiceException {
		List<Comment> result = null;
		try{
			result = commentDAO.findCommentsForNews(id);
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}
	
	public void setCommentDAO(ICommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}
	
	
}
