package com.epam.news.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.news.dao.ICommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;

import static com.epam.news.util.ColumnNames.*;

/**
 * Class that provides C.R.U.D. operations with Comment using JDBC
 *
 */
public class CommentDAOImpl implements ICommentDAO{

	private static DataSource dataSource;

	private final static String FIND_ALL = "SELECT comment_id,comment_text,creation_date,news_id FROM Comments";
	private final static String FIND_BY_ID = "SELECT comment_id,comment_text,creation_date,news_id FROM Comments WHERE comment_id=?";
	private final static String FIND_BY_NEWS_ID = "SELECT comment_id,comment_text,creation_date,news_id FROM Comments WHERE news_id=?";
	private static final String DELETE_BY_ID="DELETE FROM Comments WHERE comment_id=?";
	private static final String INSERT_BY_ENTITY="INSERT INTO Comments(comment_id,comment_text,creation_date,news_id) VALUES (COMMENTS_SEQ.nextVal,?,?,?)";
	private static final String UPDATE_BY_ID="UPDATE Comments SET comment_text=?,creation_date=?,news_id=? WHERE comment_id=?";
	private static final String DELETE_BY_NEWS_ID="DELETE FROM Comments WHERE news_id=?";

	/**
	 * Find all comments 
	 * @return List of comments
	 * @throws DAOException
	 */
	@Override
	public List<Comment> findAll() throws DAOException {
		Statement st = null;
		ResultSet rs = null;
		Connection connection = null;
		List<Comment> resultList = new ArrayList<Comment>();
		try
		{
			connection = DataSourceUtils.getConnection(dataSource);
			st = connection.createStatement();
			rs = st.executeQuery(FIND_ALL);
			while (rs.next())
			{
				Comment news = buildComment(rs);
				resultList.add(news);
			}
		}catch (SQLException e)
		{
			throw new DAOException(e);
		}
		finally
		{
			closeResultSet(rs);
			closeStatement(st);
			closeConnection(connection);
		}

		return resultList;
	}
	/**
	 * Find comment by id
	 * @param id
	 * @return comment if find is successful, returns null otherwise
	 * @throws DAOException
	 */
	@Override
	public Comment findById(Long id) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Comment result = null;
		Connection connection = null;
		try
		{
			connection = DataSourceUtils.getConnection(dataSource);
			ps=connection.prepareStatement(FIND_BY_ID);
			ps.setLong(1, id);
			rs=ps.executeQuery();

			if(rs.next())
			{
				result = buildComment(rs);
			}
		}catch(SQLException e)
		{
			throw new DAOException(e);
		}finally
		{
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return result;
	}
	/**
	 * Delete comment by id
	 * @param id of comment
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean delete(Long id) throws DAOException {
		PreparedStatement ps=null;
		Connection connection = null;
		int deleteFlag=0;
		try
		{
			connection = DataSourceUtils.getConnection(dataSource);
			ps=connection.prepareStatement(DELETE_BY_ID);
			ps.setLong(1, id);
			deleteFlag = ps.executeUpdate();
		}catch(SQLException e)
		{
			throw new DAOException(e);
		}finally
		{
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return deleteFlag == 1;
	}
	/**
	 * Create comment with given info
	 * @param entity
	 * @return id of inserted comment, 0 if insert was unsuccessful 
	 * @throws DAOException
	 */
	@Override
	public Long add(Comment entity) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection connection = null;
		Long insertId = 0L;
		try
		{
			connection = DataSourceUtils.getConnection(dataSource);
			String[] id = {"comment_id"};
			ps=connection.prepareStatement(INSERT_BY_ENTITY,id);
			ps.setString(1, entity.getCommentText());
			ps.setTimestamp(2, entity.getCreationDate());
			ps.setLong(3, entity.getNewsId());
			ps.execute();
			rs = ps.getGeneratedKeys();
			if (rs.next())
			{
				insertId = rs.getLong(1);
			}
		}catch(SQLException e)
		{
			throw new DAOException(e);
		}finally
		{
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return insertId;
	}
	/**
	 * Update comment info by id
	 * @param entity
	 * @return true if update is successful, returns false otherwise
	 * @throws DAOException
	 */
	@Override
	public boolean update(Comment entity) throws DAOException {
		int updateFlag=0;
		PreparedStatement ps = null;
		Connection connection = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			ps = connection.prepareStatement(UPDATE_BY_ID);
			ps.setString(1, entity.getCommentText());
			ps.setTimestamp(2, entity.getCreationDate());
			ps.setLong(3, entity.getNewsId());
			ps.setLong(4, entity.getId());
			updateFlag =ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return updateFlag == 1;
	}
	/**
	 * Deletes all comments for news with given id
	 * @param id of news
	 * @return true if delete is successful, returns false otherwise
	 * @throws DAOException 
	 */
	@Override
	public boolean deleteCommentsForNews(Long id) throws DAOException {
		PreparedStatement ps = null;
		Connection connection = null;
		int deleteFlag = 0;
		try
		{
			connection = DataSourceUtils.getConnection(dataSource);
			ps=connection.prepareStatement(DELETE_BY_NEWS_ID);
			ps.setLong(1, id);
			deleteFlag = ps.executeUpdate();
		}catch(SQLException e)
		{
			throw new DAOException(e);
		}finally
		{
			closePreparedStatement(ps);
			closeConnection(connection);
		}
		return deleteFlag == 1;
	}
	/**
	 * Find all comments for news with given id
	 * @param id of news
	 * @return List of comments
	 * @throws DAOException
	 */
	@Override
	public List<Comment> findCommentsForNews(Long id) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection connection = null;
		List<Comment> resultList = new ArrayList<Comment>();
		try
		{
			connection = DataSourceUtils.getConnection(dataSource);
			ps=connection.prepareStatement(FIND_BY_NEWS_ID);
			ps.setLong(1, id);
			rs=ps.executeQuery();
			while (rs.next())
			{
				Comment news = buildComment(rs);
				resultList.add(news);
			}
		}catch (SQLException e)
		{
			throw new DAOException(e);
		}
		finally
		{
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(connection);
		}

		return resultList;
	}
	/**
	 * Close prepared statement
	 * @param ps
	 * @throws DAOException
	 */
	private void closePreparedStatement(PreparedStatement ps) throws DAOException
	{
		try
		{
			if (ps != null)
			{
				ps.close();
			}
		}catch(SQLException e){
			throw new DAOException(e);
		}
	}
	/**
	 * Close statement
	 * @param st
	 * @throws DAOException
	 */
	private void closeStatement(Statement st) throws DAOException
	{
		try
		{
			if (st != null)
			{
				st.close();
			}
		}catch(SQLException e){
			throw new DAOException(e);
		}
	}
	/**
	 * Close result set
	 * @param rs
	 * @throws DAOException
	 */
	private void closeResultSet(ResultSet rs) throws DAOException
	{
		try
		{
			if (rs != null)
			{
				rs.close();
			}
		}catch(SQLException e){
			throw new DAOException(e);
		}
	}
	/**
	 * Close connection
	 * @param cn
	 * @throws DAOException
	 */
	private void closeConnection(Connection cn)
	{
		if (cn != null)
		{
			DataSourceUtils.releaseConnection(cn, dataSource);
		}
	}
	public static void setDataSource(DataSource dataSource) {
		CommentDAOImpl.dataSource = dataSource;
	}
	/**
	 * Build comment from ResultSet
	 * @throws SQLException 
	 */
	private Comment buildComment(ResultSet rs) throws SQLException
	{
		return new Comment(rs.getLong(COMMENT_ID),
				rs.getString(COMMENT_TEXT),
				rs.getTimestamp(CREATION_DATE),
				rs.getLong(NEWS_ID));
	}
	
}
