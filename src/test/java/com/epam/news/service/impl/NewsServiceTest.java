package com.epam.news.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.news.dao.impl.NewsDAOImpl;
import com.epam.news.entity.Author;
import com.epam.news.entity.News;
import com.epam.news.entity.NewsVO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.ICommentService;
import com.epam.news.service.INewsService;
import com.epam.news.service.ITagService;
import com.epam.news.service.impl.NewsServiceImpl;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;
/**
 * Test news service methods
 */

public class NewsServiceTest {
	private static final String NEWS_MANAGE_SERVICE = "newsManageService";
	private static final String NEWS_SERVICE = "newsService";
	private static Logger logger= Logger.getLogger(NewsServiceTest.class);
	private static BeanFactory factory = new ClassPathXmlApplicationContext("classpath:spring-configuration-test.xml");
	/**
	 * Configuration of log4j 
	 */
	static
	{
		ResourceBundle bundle = ResourceBundle.getBundle("resources.path",new Locale("en","EN"));
		PropertyConfigurator.configure(bundle.getString("log4j.properties"));
	}
	/**
	 * Test add news
	 * @throws Exception 
	 */
	@Test
	public void testAddNews() throws Exception
	{
		
		NewsManageServiceImpl newsServise = (NewsManageServiceImpl) factory.getBean(NEWS_MANAGE_SERVICE);

		INewsService newsService = mock(NewsServiceImpl.class);//create mocks
		ITagService tagService = mock(TagServiceImpl.class);
		NewsVO newsVO = mock(NewsVO.class);
		News news = mock(News.class);
		Author author = mock(Author.class);


		try {
			when(newsVO.getNews()).thenReturn(news);//determine the behavior of mocks
			

			when(newsVO.getTags()).thenReturn(new ArrayList<Tag>());

			when(newsVO.getAuthor()).thenReturn(author);
			when(author.getId()).thenReturn(1L);
			when(news.getId()).thenReturn(1L);

			newsServise.setNewsService(newsService);//set services mocks
			newsServise.setTagService(tagService);

			newsServise.add(newsVO);// call method from service

			verify(newsService).insertNewsAuthor(1L, 1L);//check methods calls
			verify(newsService).insertNewsTags(1L, newsVO.getTags());


		} catch ( ServiceException  e) {
			logger.error(e);
			fail("Exception");
		}
	}
	/**
	 * Test add news tags
	 */
	@Test
	public void testAddNewsTags()
	{
		NewsManageServiceImpl newsServise = (NewsManageServiceImpl) factory.getBean(NEWS_MANAGE_SERVICE);
		INewsService newsService = mock(NewsServiceImpl.class);//create mocks
		ITagService tagService = mock(TagServiceImpl.class);
		NewsVO newsVO = mock(NewsVO.class);
		News news = mock(News.class);
		try
		{
			when(newsVO.getNews()).thenReturn(news);
			when(news.getId()).thenReturn(1L);
			when(newsVO.getTags()).thenReturn(new ArrayList<Tag>());

			newsServise.setNewsService(newsService);//set services mocks
			newsServise.setTagService(tagService);
			
			newsServise.addTags(newsVO);

			verify(newsService).insertNewsTags(1L,newsVO.getTags());
		}catch ( ServiceException e) {
			logger.error(e);
			fail("Exception");
		}
	}
	/**
	 * Test delete news
	 */
	@Test
	public void testDeleteNews()
	{
		NewsManageServiceImpl newsServise = (NewsManageServiceImpl) factory.getBean(NEWS_MANAGE_SERVICE);
		INewsService newsService = mock(NewsServiceImpl.class);//create mocks
		ICommentService commentService = mock(CommentServiceImpl.class);
		NewsVO newsVO = mock(NewsVO.class);
		News news = mock(News.class);
		try {
			when(newsVO.getNews()).thenReturn(news);//determine the behavior of mocks
			when(news.getId()).thenReturn(1L);

			newsServise.setNewsService(newsService);
			newsServise.setCommentService(commentService);;

			newsServise.delete(newsVO);

			verify(newsService).deleteNewsAuthor(1L);//check methods calls
			verify(newsService).deleteNewsTags(1L);
			verify(commentService).deleteCommentsForNews(1L);
			verify(newsService).deleteNews(news);

		} catch (ServiceException  e) {
			logger.error(e);
			fail("Exception");
		}
	}
	/**
	 * Test edit news
	 */
	@Test
	public void testEditNews()
	{
		NewsServiceImpl newsServise = (NewsServiceImpl) factory.getBean(NEWS_SERVICE);
		NewsDAOImpl newsDAOImpl = mock(NewsDAOImpl.class);
		News news = mock(News.class);
		try
		{
			newsServise.setNewsDAO(newsDAOImpl);
			newsServise.editNews(news);

			verify(newsDAOImpl).update(news);
		}catch ( ServiceException | DAOException  e) {
			logger.error(e);
			fail("Exception");
		}
	}
	/**
	 * Test news by author
	 */
	@Test
	public void testNewsByAuthor()
	{
		NewsServiceImpl newsServise = (NewsServiceImpl) factory.getBean(NEWS_SERVICE);
		NewsDAOImpl newsDAOImpl = mock(NewsDAOImpl.class);
		Author author = mock(Author.class);
		try
		{
			when(newsDAOImpl.findByAuthor(author)).thenReturn(new ArrayList<News>());

			newsServise.setNewsDAO(newsDAOImpl);

			newsServise.newsByAuthor(author);

			verify(newsDAOImpl).findByAuthor(author);
		}catch ( ServiceException | DAOException  e) {
			logger.error(e);
			fail("Exception");
		}
	}
	/**
	 * Test news on page
	 */
	@Test
	public void testNewsOnPage()
	{
		NewsServiceImpl newsServise = (NewsServiceImpl) factory.getBean(NEWS_SERVICE);
		NewsDAOImpl newsDAOImpl = mock(NewsDAOImpl.class);
		try
		{
			when(newsDAOImpl.findNewsByPage(10L,1L)).thenReturn(new ArrayList<News>());

			newsServise.setNewsDAO(newsDAOImpl);

			newsServise.findNewsByPage(10L, 1L);

			verify(newsDAOImpl).findNewsByPage(10L, 1L);
		}catch ( ServiceException | DAOException  e) {
			logger.error(e);
			fail("Exception");
		}
	}
	/**
	 * Test news by tag
	 */
	@Test
	public void testNewsByTag()
	{
		NewsServiceImpl newsServise = (NewsServiceImpl) factory.getBean(NEWS_SERVICE);
		NewsDAOImpl newsDAOImpl = mock(NewsDAOImpl.class);
		List<Tag> tags = new ArrayList<>();
		try
		{
			when(newsDAOImpl.findByTags(tags)).thenReturn(new ArrayList<News>());

			newsServise.setNewsDAO(newsDAOImpl);

			newsServise.newsByTags(tags);

			verify(newsDAOImpl).findByTags(tags);
		}catch ( ServiceException | DAOException  e) {
			logger.error(e);
			fail("Exception");
		}
	}
	/**
	 * Test view single news
	 */
	@Test
	public void testViewSingleNews()
	{
		NewsServiceImpl newsServise = (NewsServiceImpl) factory.getBean(NEWS_SERVICE);
		NewsDAOImpl newsDAOImpl = mock(NewsDAOImpl.class);
		try
		{
			when(newsDAOImpl.findById(1L)).thenReturn(null);

			newsServise.setNewsDAO(newsDAOImpl);

			newsServise.viewSingleNews(1L);

			verify(newsDAOImpl).findById(1L);
		}catch ( ServiceException | DAOException  e) {
			logger.error(e);
			fail("Exception");
		}
	}
	/**
	 * Test most commented news
	 */
	@Test
	public void testMostCommentedNews()
	{
		NewsServiceImpl newsServise = (NewsServiceImpl) factory.getBean(NEWS_SERVICE);
		NewsDAOImpl newsDAOImpl = mock(NewsDAOImpl.class);
		try
		{
			when(newsDAOImpl.mostCommentedNews()).thenReturn(new ArrayList<News>());

			newsServise.setNewsDAO(newsDAOImpl);
			newsServise.mostCommented();

			verify(newsDAOImpl).mostCommentedNews();
		}catch ( ServiceException | DAOException  e) {
			logger.error(e);
			fail("Exception");
		}
	}
}
