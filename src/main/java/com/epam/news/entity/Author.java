package com.epam.news.entity;

import java.io.Serializable;
import java.sql.Timestamp;
/**
 * CLass to store author info
 *
 */
public class Author implements Serializable{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7471930922830752352L;
	
	/**
	 * Author id
	 */
	private Long id;
	/**
	 * Author name
	 */
	private String name;
	/**
	 * if author expired or not (null if not)
	 */
	private Timestamp expired;
	
	public Author() {}
	public Author(Long id, String name, Timestamp expired) {
		super();
		this.id = id;
		this.name = name;
		this.expired = expired;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Timestamp getExpired() {
		return expired;
	}
	public void setExpired(Timestamp expired) {
		this.expired = expired;
	}
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("Author [id=");
		str.append(id);
		str.append(", name=" );
		str.append(name);
		str.append(", expired=");
		str.append(expired);
		str.append("]");
		return str.toString();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
