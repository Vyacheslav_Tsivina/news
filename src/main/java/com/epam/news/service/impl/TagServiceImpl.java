package com.epam.news.service.impl;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.ITagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.ITagService;
/**
 * Class that provides actions with tags
 *
 */
@Transactional
public class TagServiceImpl implements ITagService{

	static private Logger logger= Logger.getLogger(TagServiceImpl.class);
	private ITagDAO tagDAO;

	/**
	 * Configuration of log4j 
	 */
	static
	{
		ResourceBundle bundle = ResourceBundle.getBundle("resources.path",new Locale("en","EN"));
		PropertyConfigurator.configure(bundle.getString("log4j.properties"));
	}

	/**
	 * Add new tag and set inserted id into tag
	 * @param tag to be added
	 */
	@Override
	public void addTag(Tag tag) throws ServiceException {
		try{
			tag.setId(tagDAO.add(tag));
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}
	/**
	 * Delete given tag by comment id
	 * @param tag to be deleted
	 */
	@Override
	public void deleteTag(Tag tag) throws ServiceException {
		try{
			tagDAO.delete(tag.getId());
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}
	/**
	 * Update given tag by comment id
	 * @param tag to be updated
	 * @throws ServiceException
	 */
	@Override
	public void updateTag(Tag tag) throws ServiceException {
		try{
			tagDAO.update(tag);
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}

	}
	/**
	 * Find tag by id
	 * @param id
	 * @return tag or null
	 * @throws ServiceException
	 */
	@Override
	public Tag findTag(Long id) throws ServiceException {
		Tag tag = null;
		try{
			tag = tagDAO.findById(id);
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return tag;
	}
	/**
	 * Find all tags for news with given id
	 * @param id of news
	 * @return list of tags
	 * @throws ServiceException
	 */
	@Override
	public List<Tag> findTagsForNews(Long id) throws ServiceException {
		List<Tag> result = null;
		try
		{
			result = tagDAO.findTagsForNews(id);
		}catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
		return result;
	}
	public void setTagDAO(ITagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}


}
