package com.epam.news.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.news.dao.INewsDAO;
import com.epam.news.entity.Author;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;

import static com.epam.news.util.ColumnNames.*;

/**
 * Interface that provides C.R.U.D. operations with News and related information using JDBC
 *
 */
public class NewsDAOImpl implements INewsDAO{

	private DataSource dataSource;

	private final static String FIND_ALL = "SELECT news_id,short_text,full_text,title,creation_date,modification_date FROM News";
	private final static String FIND_NEWS_BY_PAGE="SELECT news_id,short_text,full_text,title,creation_date,modification_date FROM "+
			"( SELECT news_id,short_text,full_text,title,creation_date,modification_date,Row_Number() OVER (ORDER BY CREATION_DATE) MyRow FROM NEWS)"+
			"WHERE MyRow BETWEEN ? AND ?";
	private final static String FIND_BY_ID = "SELECT news_id,short_text,full_text,title,creation_date,modification_date FROM News WHERE news_id=?";
	private final static String IS_TAG_FOR_NEWS = "SELECT tag_id FROM News_tag WHERE news_id=? AND tag_id=?";
	private static final String DELETE_BY_ID="DELETE FROM News WHERE news_id=?";
	private static final String INSERT_BY_ENTITY="INSERT INTO News(news_id,short_text,full_text,title,creation_date,modification_date) VALUES (NEWS_SEQ.nextVal,?,?,?,?,?)";
	private static final String INSERT_NEWS_AUTHOR="INSERT INTO News_author(news_author_id,news_id,author_id) VALUES(NEWS_AUTHOR_SEQ.nextVal,?,?)";
	private static final String INSERT_NEWS_TAG="INSERT INTO News_tag(news_tag_id,news_id,tag_id) VALUES(NEWS_TAG_SEQ.nextVal,?,?)";
	private static final String DELETE_NEWS_TAGS="DELETE FROM News_tag WHERE News_id=?";
	private static final String DELETE_NEWS_AUTHOR="DELETE FROM News_author WHERE News_id=?";
	private static final String UPDATE_BY_ID="UPDATE News SET short_text=?,full_text=?,title=?"
			+ ",creation_date=?,modification_date=? WHERE news_id=?";
	private final static String FIND_NEWS_BY_AUTHOR = "SELECT NEWS.NEWS_ID,"
			+ "NEWS.SHORT_TEXT,"
			+ "NEWS.FULL_TEXT,"
			+ "NEWS.TITLE,"
			+ "NEWS.CREATION_DATE,"
			+ "NEWS.MODIFICATION_DATE "
			+ "FROM NEWS "
			+ "JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID "
			+ "WHERE NEWS_AUTHOR.AUTHOR_ID=?";

	private final static String MOST_COMMENTED_NEWS ="SELECT NEWS.NEWS_ID,"
			+ "NEWS.SHORT_TEXT,"
			+ "NEWS.FULL_TEXT,"
			+ "NEWS.TITLE,"
			+ "NEWS.CREATION_DATE,"
			+ "NEWS.MODIFICATION_DATE "
			+ "FROM NEWS "
			+ "LEFT JOIN (SELECT news_id, COUNT(*) AS comments_count FROM COMMENTS GROUP BY news_id) T ON (news.news_id = T.news_id)"
			+ "ORDER BY -comments_count, NEWS.MODIFICATION_DATE DESC";
		
		
		/**
		 * Find list of all news
		 * @return list of news
		 * @throws DAOException 
		 */
		@Override
		public List<News> findAll() throws DAOException {
			Statement st = null;
			ResultSet rs = null;
			Connection connection = null;
			List<News> resultList = new ArrayList<News>();
			try
			{
				connection = DataSourceUtils.getConnection(dataSource);
				st = connection.createStatement();
				rs = st.executeQuery(FIND_ALL);
				while (rs.next())
				{
					News news = buildNews(rs);
					resultList.add(news);
				}
			}catch (SQLException e)
			{
				throw new DAOException(e);
			}
			finally
			{
				closeResultSet(rs);
				closeStatement(st);
				closeConnection(connection);
			}

			return resultList;
		}
		/**
		 * 
		 * @param id of news
		 * @return NewsEntity if method is successful, returns null otherwise
		 * @throws DAOException 
		 */
		@Override
		public News findById(Long id) throws DAOException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			Connection connection = null;
			News result = null;
			try
			{
				connection = DataSourceUtils.getConnection(dataSource);
				ps=connection.prepareStatement(FIND_BY_ID);
				ps.setLong(1, id);
				rs=ps.executeQuery();

				if(rs.next())
				{
					result = buildNews(rs);
				}
			}catch(SQLException e)
			{
				throw new DAOException(e);
			}finally
			{
				closeResultSet(rs);
				closePreparedStatement(ps);
				closeConnection(connection);
			}
			return result;
		}
		/**
		 * 
		 * @param id of news
		 * @return true if delete is successful, returns false otherwise
		 * @throws DAOException 
		 */
		@Override
		public boolean delete(Long id) throws DAOException {
			PreparedStatement ps = null;
			Connection connection = null;
			int deleteFlag = 0;
			try
			{
				connection = DataSourceUtils.getConnection(dataSource);
				ps=connection.prepareStatement(DELETE_BY_ID);
				ps.setLong(1, id);
				deleteFlag = ps.executeUpdate();
			}catch(SQLException e)
			{
				throw new DAOException(e);
			}finally
			{
				closePreparedStatement(ps);
				closeConnection(connection);
			}
			return deleteFlag == 1;
		}


		/**
		 * Create news with given info
		 * @param entity
		 * @return id of inserted comment, 0 if insert was unsuccessful 
		 * @throws DAOException 
		 */
		@Override
		public Long add(News entity) throws DAOException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			Connection connection = null;
			Long insertId = 0L;

			if (entity == null) return -1L;
			try
			{
				String[] id = {"news_id"};
				connection = DataSourceUtils.getConnection(dataSource);
				ps=connection.prepareStatement(INSERT_BY_ENTITY, id);
				ps.setString(1, entity.getShortText());
				ps.setString(2, entity.getFullText());
				ps.setString(3, entity.getTitle());
				ps.setTimestamp(4, entity.getCreationDate());
				ps.setTimestamp(5, entity.getModificationDate());
				ps.execute();
				rs = ps.getGeneratedKeys();
				if (rs.next())
				{
					insertId = rs.getLong(1);
				}
			}catch(SQLException e)
			{
				throw new DAOException(e);
			}finally
			{
				closeResultSet(rs);
				closePreparedStatement(ps);
				closeConnection(connection);
			}
			return insertId;
		}

		/**
		 * Update news info by id
		 * @param entity
		 * @return true if update is successful, returns false otherwise
		 * @throws DAOException 
		 */
		@Override
		public boolean update(News entity) throws DAOException {
			int updateFlag=0;
			PreparedStatement ps = null;
			Connection connection = null;

			if (entity == null) return false;
			try {
				connection = DataSourceUtils.getConnection(dataSource);
				ps = connection.prepareStatement(UPDATE_BY_ID);
				ps.setString(1, entity.getShortText());
				ps.setString(2, entity.getFullText());
				ps.setString(3, entity.getTitle());
				ps.setTimestamp(4, entity.getCreationDate());
				ps.setTimestamp(5, entity.getModificationDate());
				ps.setLong(6, entity.getId());
				updateFlag =ps.executeUpdate();
			} catch (SQLException e) {
				throw new DAOException(e);
			}finally {
				closePreparedStatement(ps);
				closeConnection(connection);
			}
			return updateFlag==1;
		}



		/**
		 * Insert info about news author
		 * @param newsId 
		 * @param authorId 
		 * @return true if insert is successful, returns false otherwise
		 * @throws DAOException 
		 */
		@Override
		public boolean insertNewsAuthor(Long newsId,Long authorId) throws DAOException {
			PreparedStatement ps = null;
			Connection connection = null;
			boolean insertFlag = false;
			try
			{
				connection = DataSourceUtils.getConnection(dataSource);
				ps=connection.prepareStatement(INSERT_NEWS_AUTHOR);
				ps.setLong(1, newsId);
				ps.setLong(2, authorId);
				insertFlag = ps.execute();
			}catch(SQLException e)
			{
				throw new DAOException(e);
			}finally
			{
				closePreparedStatement(ps);
				closeConnection(connection);
			}
			return insertFlag;
		}

		/**
		 * Check if there is already such tag(tags) for news and then insert it if it needed
		 * @param newsId news id
		 * @param tags ArrayList of tags to be added
		 * @throws DAOException 
		 */
		@Override
		public void insertNewsTags(Long newsId,List<Tag> tags) throws DAOException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			PreparedStatement check = null;
			Connection connection = null;
			try
			{
				connection = DataSourceUtils.getConnection(dataSource);
				check = connection.prepareStatement(IS_TAG_FOR_NEWS);
				check.setLong(1,newsId);
				ps=connection.prepareStatement(INSERT_NEWS_TAG);
				for (Tag tag:tags)
				{
					check.setLong(2,tag.getId());
					rs = check.executeQuery();//if there is no such tag for news
					if (!rs.next())
					{
						ps.setLong(1, newsId);
						ps.setLong(2, tag.getId());
						ps.execute();
						closeResultSet(rs);
					}
				}
			}catch(SQLException e)
			{
				throw new DAOException(e);
			}finally
			{
				closeResultSet(rs);
				closePreparedStatement(ps);
				closeConnection(connection);
			}

		}

		/**
		 * Find all news from given author
		 * @param author
		 * @return List of all news by author
		 * @throws DAOException 
		 */
		public List<News> findByAuthor(Author author) throws DAOException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			Connection connection = null;
			List<News> resultList = new ArrayList<News>();
			if (author == null) return null;

			try
			{
				connection = DataSourceUtils.getConnection(dataSource);
				ps = connection.prepareStatement(FIND_NEWS_BY_AUTHOR);
				ps.setLong(1,author.getId());
				rs = ps.executeQuery();
				while (rs.next())
				{
					News news = buildNews(rs);
					resultList.add(news);
				}
			}catch (SQLException e)
			{
				throw new DAOException(e);
			}
			finally
			{
				closeResultSet(rs);
				closePreparedStatement(ps);
				closeConnection(connection);
			}

			return resultList;
		}

		/**
		 * Find news by given tags
		 * @param tags
		 * @return return List of all news by tags
		 * @throws DAOException 
		 */
		@Override
		public List<News> findByTags(List<Tag> tags) throws DAOException
		{
			List<News> resultList = new ArrayList<News>();
			PreparedStatement ps = null;
			ResultSet rs = null;
			Connection connection = null;
			if (tags.size() == 0) return resultList;
			try
			{
				connection = DataSourceUtils.getConnection(dataSource);
				
				StringBuilder sql= new StringBuilder();//build sql
				sql.append("SELECT NEWS.NEWS_ID,");
				sql.append("NEWS.SHORT_TEXT,");
				sql.append("NEWS.FULL_TEXT,");
				sql.append("NEWS.TITLE,");
				sql.append("NEWS.CREATION_DATE,");
				sql.append("NEWS.MODIFICATION_DATE ");
				sql.append("FROM NEWS ");
				sql.append("WHERE");
				sql.append("  NEWS.news_id IN (");
				sql.append("SELECT NEWS_TAG.news_id ");
				sql.append("FROM NEWS_TAG ");
				sql.append("WHERE ");
				sql.append("NEWS_TAG.tag_id IN (? ");
				for (int i=0;i<tags.size()-1;i++)//add so many parameters as we need
				{
					sql.append(",?");
				}
				sql.append(") ");
				sql.append("GROUP BY NEWS_TAG.news_id ");
				sql.append("HAVING COUNT(NEWS_TAG.tag_id) = ?)");
				ps=connection.prepareStatement(sql.toString());
				
				for (int i=0;i<tags.size();i++)
				{
					ps.setLong(i+1, tags.get(i).getId());//add tags id
				}
				ps.setInt(tags.size()+1, tags.size());//add tags count
				rs=ps.executeQuery();
				while (rs.next())
				{
					News news = buildNews(rs);
					resultList.add(news);
				}
				
			}catch(SQLException e)
			{
				throw new DAOException(e);
			}finally
			{
				closeResultSet(rs);
				closePreparedStatement(ps);
				closeConnection(connection);
			}
			return resultList;
		}
		/**
		 * Deletes all tags for news 
		 * @param id of news
		 * @return true if delete is successful, returns false otherwise
		 * @throws DAOException 
		 */
		@Override
		public boolean deleteNewsTags(Long id) throws DAOException {
			PreparedStatement ps = null;
			Connection connection = null;
			int deleteFlag = 0;
			try
			{
				connection = DataSourceUtils.getConnection(dataSource);
				ps=connection.prepareStatement(DELETE_NEWS_TAGS);
				ps.setLong(1, id);
				deleteFlag = ps.executeUpdate();
			}catch(SQLException e)
			{
				throw new DAOException(e);
			}finally
			{
				closePreparedStatement(ps);
				closeConnection(connection);
			}
			return deleteFlag == 1;
		}
		/**
		 * 
		 * @param id of news
		 * @return true if delete is successful, returns false otherwise
		 * @throws DAOException 
		 */
		@Override
		public boolean deleteNewsAuthor(Long id) throws DAOException {
			PreparedStatement ps = null;
			Connection connection = null;
			int deleteFlag = 0;
			try
			{
				connection = DataSourceUtils.getConnection(dataSource);
				ps=connection.prepareStatement(DELETE_NEWS_AUTHOR);
				ps.setLong(1, id);
				deleteFlag = ps.executeUpdate();
			}catch(SQLException e)
			{
				throw new DAOException(e);
			}finally
			{
				closePreparedStatement(ps);
				closeConnection(connection);
			}
			return deleteFlag == 1;
		}

		/**
		 * 
		 * @return List of all news sorted by number of comments
		 * @throws DAOException 
		 */
		@Override
		public List<News> mostCommentedNews() throws DAOException {
			PreparedStatement ps = null;
			Connection connection = null;
			ResultSet rs = null;
			List<News> resultList = new ArrayList<News>();
			try
			{
				connection = DataSourceUtils.getConnection(dataSource);
				ps = connection.prepareStatement(MOST_COMMENTED_NEWS);
				rs = ps.executeQuery();
				while (rs.next())
				{
					News news = buildNews(rs);
					resultList.add(news);
				}
			}catch (SQLException e)
			{
				throw new DAOException(e);
			}
			finally
			{
				closeResultSet(rs);
				closePreparedStatement(ps);
				closeConnection(connection);
			}

			return resultList;
		}
		/**
		 * Return list of news on given page number
		 * pages number begins with 1. News ordered by creation date;
		 * @param newsOnPage how many news will be on page
		 * @param pageNumber determine number of page which should be return
		 * @return list of news on given page
		 * @throws DAOException
		 */
		@Override
		public List<News> findNewsByPage(Long newsOnPage, Long pageNumber) throws DAOException {
			PreparedStatement ps = null;
			Connection connection = null;
			ResultSet rs = null;
			List<News> resultList = new ArrayList<News>();
			try
			{
				connection = DataSourceUtils.getConnection(dataSource);
				ps = connection.prepareStatement(FIND_NEWS_BY_PAGE);
				ps.setLong(1, 1+newsOnPage*(pageNumber-1));//first news on page
				ps.setLong(2, newsOnPage*(pageNumber));//last news on page
				rs = ps.executeQuery();
				while (rs.next())
				{
					News news = buildNews(rs);
					resultList.add(news);
				}
			}catch (SQLException e)
			{
				throw new DAOException(e);
			}
			finally
			{
				closeResultSet(rs);
				closePreparedStatement(ps);
				closeConnection(connection);
			}

			return resultList;
		}
		/**
		 * Close prepared statement
		 * @param ps
		 * @throws DAOException
		 */
		private void closePreparedStatement(PreparedStatement ps) throws DAOException
		{
			try
			{
				if (ps != null)
				{
					ps.close();
				}
			}catch(SQLException e){
				throw new DAOException(e);
			}
		}
		/**
		 * Close statement
		 * @param st
		 * @throws DAOException
		 */
		private void closeStatement(Statement st) throws DAOException
		{
			try
			{
				if (st != null)
				{
					st.close();
				}
			}catch(SQLException e){
				throw new DAOException(e);
			}
		}
		/**
		 * Close result set
		 * @param rs
		 * @throws DAOException
		 */
		private void closeResultSet(ResultSet rs) throws DAOException
		{
			try
			{
				if (rs != null)
				{
					rs.close();
				}
			}catch(SQLException e){
				throw new DAOException(e);
			}
		}
		/**
		 * Close connection
		 * @param cn
		 * @throws DAOException
		 */
		private void closeConnection(Connection cn)
		{
			if (cn != null)
			{
				DataSourceUtils.releaseConnection(cn, dataSource);
			}
		}

		public void setDataSource(DataSource dataSource) {
			this.dataSource = dataSource;
		}
		/**
		 * Build news from ResultSet
		 * @throws SQLException 
		 */
		private News buildNews(ResultSet rs) throws SQLException
		{
			return new News(rs.getLong(NEWS_ID),
					rs.getString(SHORT_TEXT),
					rs.getString(FULL_TEXT),
					rs.getString(TITLE),
					rs.getTimestamp(CREATION_DATE),
					rs.getTimestamp(MODIFICATION_DATE));
		}
}
