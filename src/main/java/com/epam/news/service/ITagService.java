package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;

/**
 * Interface that provides actions with tags
 *
 */
public interface ITagService {
	/**
	 * Add new tag and set inserted id into tag
	 * @param tag to be added
	 */
	void addTag(Tag tag) throws ServiceException;
	/**
	 * Delete given tag by tag id
	 * @param tag to be deleted
	 */
	void deleteTag(Tag tag) throws ServiceException;
	/**
	 * Update given tag by tag id
	 * @param tag to be updated
	 * @throws ServiceException
	 */
	void updateTag(Tag tag) throws ServiceException;
	/**
	 * Find tag by id
	 * @param id
	 * @return tag or null
	 * @throws ServiceException
	 */
	Tag findTag(Long id) throws ServiceException;

	/**
	 * Find all tags for news with given id
	 * @param id of news
	 * @return list of tags
	 * @throws ServiceException
	 */
	List<Tag> findTagsForNews(Long id) throws ServiceException;
}
