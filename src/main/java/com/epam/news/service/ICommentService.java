package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.Comment;
import com.epam.news.exception.ServiceException;

/**
 * Interface that provides actions with comments
 *
 */
public interface ICommentService {
	/**
	 * Add new comment and set inserted id into comment
	 * @param comment to be added
	 */
	void addComment(Comment comment) throws ServiceException;
	/**
	 * Delete given comment by comment id
	 * @param comment to be deleted
	 */
	void deleteComment(Comment comment) throws ServiceException;
	/**
	 * Update given comment by comment id
	 * @param comment to be updated
	 * @throws ServiceException
	 */
	void updateComment(Comment comment) throws ServiceException;
	/**
	 * Find comment by id
	 * @param id
	 * @return Comment or null
	 * @throws ServiceException
	 */
	Comment findComment(Long id) throws ServiceException;
	
	/**
	 * Deletes all comments for news with given id
	 * @param id of news
	 * @throws ServiceException 
	 */
	void deleteCommentsForNews(Long id) throws ServiceException;
	/**
	 * Find all comments for news with given id
	 * @param id of news
	 * @return List of comments
	 * @throws ServiceException
	 */
	List<Comment> findCommentsForNews(Long id) throws ServiceException;
}
